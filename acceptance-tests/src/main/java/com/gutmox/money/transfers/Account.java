package com.gutmox.money.transfers;

public class Account {

    String name, currency, state, balance;

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        String name, currency, state, balance;

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withCurrency(String currency) {
            this.currency = currency;
            return this;
        }

        public Builder withState(String state) {
            this.state = state;
            return this;
        }

        public Builder withBalance(String balance) {
            this.balance = balance;
            return this;
        }

        public Account build() {
            Account account = new Account();
            account.state = this.state;
            account.currency = this.currency;
            account.balance = this.balance;
            account.name = this.name;
            return account;
        }
    }

    public String toJson() {
        return "{" +
                "\"name\":\"" + name + '\"' +
                ", \"currency\":\"" + currency + '\"' +
                ", \"state\":\"" + state + '\"' +
                ", \"balance\":" + balance +
                '}';
    }
}
