package com.gutmox.money.transfers;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.buffer.Buffer;
import io.vertx.rxjava.core.http.HttpClientRequest;
import rx.Observable;

public class VertxHttpClient {

    private Vertx vertx;

    public VertxHttpClient(Vertx vertx) {
        this.vertx = vertx;
    }

    public Observable<JsonArray> invokeService(String host, String uri) {
        Observable<JsonArray> observable = Observable.unsafeCreate(data -> {
            io.vertx.rxjava.core.http.HttpClient client = vertx.createHttpClient();
            HttpClientRequest req = client.request(HttpMethod.GET, host, uri);
            req.toObservable().
                    // Status code check and -> Observable<Buffer>
                            flatMap(resp -> {
                        if (resp.statusCode() != 200) {
                            throw new RuntimeException("Wrong status code " + resp.statusCode());
                        }
                        return Observable.just(Buffer.buffer()).mergeWith(resp.toObservable());
                    }).
                    // Reduce all buffers in a single buffer
                            reduce(Buffer::appendBuffer).
                    // Turn in to a string
                            map(buffer -> buffer.toJsonArray()).
                    // Get a single buffer
                            subscribe(data);
            // End request
            req.end();
        });
        return observable;
    }
}
