package com.gutmox.money.transfers.traits

import com.gutmox.money.transfers.verticles.MainVerticle
import io.vertx.core.DeploymentOptions
import io.vertx.core.http.RequestOptions
import io.vertx.core.json.JsonObject
import org.junit.After
import org.junit.Before
import spock.util.concurrent.AsyncConditions

trait Vertx {

    io.vertx.rxjava.core.Vertx vertx = io.vertx.rxjava.core.Vertx.vertx()

    int port

    AsyncConditions serverConditions

    @Before
    def setupUserSpec() {
        println "Calling setup in Trait..."
        ServerSocket socket = new ServerSocket(0);
        port = socket.getLocalPort();
        socket.close();
        DeploymentOptions options = new DeploymentOptions()
                .setConfig(new JsonObject()
                .put("http.port", port)
        );
        serverConditions = new AsyncConditions(1);

        vertx.deployVerticle(MainVerticle.class.getName(), options)
                { deployResponse ->

                    serverConditions.evaluate {
                        assert deployResponse.succeeded()
                    }
                };
        serverConditions.await(5)
        println "setupSpec"
    }


    @After
    def cleanupUserSpec() {
        println "Calling cleanup in Trait..."
        vertx.close()
    }

    def RequestOptions httpOptions(String uri) {
        def opts = new RequestOptions()
        opts.port = port
        opts.host = 'localhost'
        opts.URI = uri
        opts
    }
}