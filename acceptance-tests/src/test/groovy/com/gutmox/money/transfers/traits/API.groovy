package com.gutmox.money.transfers.traits

import com.gutmox.money.transfers.Account
import io.vertx.core.http.RequestOptions
import io.vertx.rxjava.core.http.HttpClient

trait API {

    void createAccount(HttpClient client, RequestOptions opts, Account account, async) {
        client.post(opts)
                .putHeader("content-type", "application/json")
                .putHeader("content-length", account.toJson().length().toString())
                .handler({ res ->
            async.evaluate {
                assert res.statusCode() == 200
            }
            res.bodyHandler({ body ->
                async.evaluate {
                    assert body.toString().toLowerCase()
                            .contains(
                            account.toJson().substring(0, account.toJson().length() - 1).toLowerCase().replaceAll("\\s+", "")
                    )
                }
            })
        })
                .write(account.toJson()).end()
    }
}