package com.gutmox.money.transfers

import io.vertx.core.http.RequestOptions
import spock.lang.Specification
import spock.util.concurrent.AsyncConditions

class TransfersTest extends Specification implements
        com.gutmox.money.transfers.traits.Vertx,
        com.gutmox.money.transfers.traits.API {

    def "should make transfer between accounts"() {

        given: "An async HTTP client"
        def client = vertx.createHttpClient()
        RequestOptions accountOpts = httpOptions('/accounts')
        def async = new AsyncConditions(2)

        and: "2 accounts"

        def account1 = Account.builder().withName("acc1").withBalance("10").withCurrency("EUR").withState("active").build()
        createAccount(client, accountOpts, account1, async)
        async.await(10)

        def account2 = Account.builder().withName("acc2").withBalance("15").withCurrency("EUR").withState("active").build()
        createAccount(client, accountOpts, account2, async)
        async.await(10)


        when: "make a transfer"
        println "client calling"

        RequestOptions opts = httpOptions('/transfers')

        client.getNow(opts, { res ->
            async.evaluate {
                assert res.statusCode() == 200
            }
            res.bodyHandler({ body ->
                async.evaluate {
                    assert body.toString().toLowerCase() == "{\"request_identifier\": 1,\"source_account_id\": \"AT021904535649333520,\"target_account_id\": \"AT191904371872473225\",\"amount\": 5,\"currency\": \"EUR\", \"reference\": \"Transaction\",\"transaction_id\": \"0d50366a-d0e7-41a3-8429-f07992671681\"}"
                }
            })
        })

        then: "Expect the result to be completed in the specified time"
        async.await(10)

    }
}
