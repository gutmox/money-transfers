package com.gutmox.money.transfers

import io.vertx.core.http.RequestOptions
import spock.lang.Specification
import spock.util.concurrent.AsyncConditions

class StatusTest extends Specification implements com.gutmox.money.transfers.traits.Vertx{

    def "should return status"() {

        given: "An async HTTP client"
        println "initialising client"
        def client = vertx.createHttpClient()
        and: "Some HTTP request options"
        println "setting options client"
        RequestOptions opts = httpOptions('/status')
        and: "An instance of AsyncConditions"
        def async = new AsyncConditions(2)
        when: "Status is requested"
        println "client calling"
        client.getNow(opts, { res ->
            async.evaluate {
                assert res.statusCode() == 200
            }
            res.bodyHandler({ body ->
                async.evaluate {
                    assert body.toString().toLowerCase() == "{\"status\":\"show must go on\"}"
                }
            })
        })

        then: "Expect the result to be completed in the specified time"
        async.await(10)
    }
}
