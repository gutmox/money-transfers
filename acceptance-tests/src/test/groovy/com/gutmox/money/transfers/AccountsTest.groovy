package com.gutmox.money.transfers

import io.vertx.core.http.RequestOptions
import io.vertx.rxjava.core.http.HttpClient
import spock.lang.Specification
import spock.util.concurrent.AsyncConditions

class AccountsTest extends Specification implements
        com.gutmox.money.transfers.traits.Vertx,
        com.gutmox.money.transfers.traits.API {

    def "should create accounts"() {
        println "createHttpClient"
        given: "An async HTTP client"
        def client = vertx.createHttpClient()
        RequestOptions opts = httpOptions('/accounts')
        def async = new AsyncConditions(2)
        and: "An account"
        def account = Account.builder().withName(name).withBalance(balance).withCurrency(currency).withState(state).build()
        println account.toJson()
        expect: "The API Returns ann account created"
        createAccount(client, opts, account, async)

        async.await(10)

        where:
        name   | balance | currency | state
        "acc1" | "10"    | "EUR"    | "active"
        "acc2" | "15"    | "EUR"    | "active"
    }
}
