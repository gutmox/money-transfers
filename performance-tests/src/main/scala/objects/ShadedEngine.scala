package objects

import io.gatling.app.Gatling
import io.gatling.core.config.GatlingPropertiesBuilder

object ShadedEngine extends App {
  val simulation = if (args.length > 0) args(0) else null
  val props = new GatlingPropertiesBuilder
  props.dataDirectory("jar")
  props.simulationClass(simulation)
  props.resultsDirectory("build/gatling-results")
  sys.exit(Gatling.fromMap(props.build))
}