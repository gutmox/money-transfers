package com.gutmox.performance

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

class StatusSimulation extends Simulation {

  val baseUrl = "http://localhost:8080"

  val httpConf = http
    .baseURL(baseUrl)
    .acceptHeader("application/json")
    .shareConnections

  val headers = Map(
    "Content-Type" -> "application/json",
    "Accept" -> "application/json")


  val hello = scenario("Status")
    .exec(exec(http("status")
      .get("/status")
      .headers(headers)
      .asJSON
      .check(status.is(200))
    ))

  setUp(
    hello
      .inject(
        constantUsersPerSec(100) during (20 seconds)
      )
  ).protocols(httpConf)
}
