#!/usr/bin/env bash

export DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

gradle clean build -p $DIR

tar -xvf $DIR/build/distributions/performance-tests.tar -C $DIR/build/distributions/

$DIR/build/distributions/performance-tests/bin/performance-tests com.gutmox.performance.StatusSimulation

