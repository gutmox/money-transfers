# Foundation test bed hello performance

## Usage example

### with gradle

```
gradle perfTest

```

### Command line

```
./build-and-run-hello.sh

```

### Docker

```
docker build -t foundation-test-bed-hello .
```

```
docker run -t -i foundation-test-bed-hello
```