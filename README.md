# Money Transfers Simple RESTful API

[![pipeline status](https://gitlab.com/gutmox/money-transfers/badges/master/pipeline.svg)](https://gitlab.com/gutmox/money-transfers/commits/master)

[![coverage report](https://gitlab.com/gutmox/money-transfers/badges/master/coverage.svg)](https://gitlab.com/gutmox/money-transfers/commits/master)

RESTful API for money transfers between accounts. 

## Swagger API Definition and manual tests

* https://app.swaggerhub.com/apis/gutmox/Money_Transfer/1.0.0

* When the server is running, just open a browser to: `http://localhost:8080` to nagivate to the swagger ui

This design has been deeply inspired / following the Open Banking project specifications 

![Swagger definition](https://gitlab.com/gutmox/money-transfers/raw/master/money-transfers-service/src/main/resources/snapshot.png)  
 

## Modules Layout

```
.
├── gradle                              - gradle wrapper
│   └── wrapper
├── acceptance-tests                    - Acceptance tests with Spock and Groovy
├── money-transfers-service             - Microservice to manage accounts and transfers
└── performance tests                   - Basic Gatling performance tests 

```

## High Level view

```mermaid
graph TD
Transfers-Service -->|Create Account| F[fa:fa-database Accounts]
Transfers-Service -->|Do Transfer| E[fa:fa-database Transactions]
```

## Sequence

```mermaid
sequenceDiagram
    participant Client
    participant Accounts
    participant Transfers
    participant Transaction
    Client->> Accounts: create account 1
    Accounts-->> Accounts: data validations
    Client->> Accounts: create account 2
    Accounts-->> Accounts: data validations
    Client->> Transfers: doTransfer
    Transfers-->> Accounts: tx validations
    Transfers->> Transaction: Store transaction details
    Note right of Transaction: Store all <br/> transactions
    Transfers-->> Accounts: check source account balance > tx amount       
    Transfers->> Accounts: add amount to Target Account 
    Transfers->> Accounts: subtract amount from Source Account 
    Transfers->> Client: "Transaction Identifier"
```

## Built With

* [Java 8](http://www.oracle.com/technetwork/java/javase/10-relnote-issues-4108729.html) - Code language 
* [Vert.x](https://vertx.io) - REST library used
* [Gradle](https://gradle.org) - Build tool

### Unit Testing

* [JUnit 4](https://junit.org/junit4/) - Junit
* [Mockito](http://site.mockito.org)   - Mocking 
* [AssertJ](http://joel-costigliola.github.io/assertj/index.html) - adds fluent assertion support

### Acceptance Testing

* [Spock](http://spockframework.org) - Acceptance tests framework

### Performance Testing

* [Gatling](https://gatling.io) - Performance tests framework



## Running It

### From the IDE, run the main class:

```
    com.gutmox.money.transfers.Runner

```

## Running tests

### Unit tests

```
    gradle clean test -p money-transfers-service
```

### Acceptance tests

```
    gradle clean execMongo test stopMongo -p acceptance-tests
```

## Building
```
    gradle clean build
```

## Docker

### To build:

```
    docker build -t money-transfer-service ./money-transfer-service
 ```
### To run:

```
    docker run -t -i -p 8080:8080 money-transfer-service
```

## Sonar

### Report

[sonarcloud report](https://sonarcloud.io/dashboard?id=money-transfers)

### Running it locally

``` 
 gradle sonarqube \
  -Dsonar.organization=gutmox-github \
  -Dsonar.host.url=https://sonarcloud.io \
  -Dsonar.login=xxxx

```