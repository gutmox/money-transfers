#!/usr/bin/env bash

docker build -t money-transfers-service ./money-transfers-service

docker run -t -i -p 8080:8080 money-transfers-service