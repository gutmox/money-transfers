package com.gutmox.money.transfers.api.domain;

public enum Currency {
    EUR,
    GBP
}
