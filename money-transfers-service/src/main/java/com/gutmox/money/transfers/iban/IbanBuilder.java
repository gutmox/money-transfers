package com.gutmox.money.transfers.iban;

import io.netty.util.internal.ThreadLocalRandom;
import org.iban4j.CountryCode;
import org.iban4j.Iban;

public class IbanBuilder {


    public String getNextIbanAccount(){
        Iban iban = new Iban.Builder()
                .countryCode(CountryCode.AT)
                .bankCode("1904")
                .accountNumber("" + generateId())
                .buildRandom();
        return iban.toString();
    }

    private long generateId() {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        return random.nextLong(100_000_000_000L, 1_000_000_000_000L);
    }
}
