package com.gutmox.money.transfers.api.validations;

import com.gutmox.money.transfers.api.domain.Currency;

class CurrencyValidation {

    private CurrencyValidation() {
    }

    static boolean isCurrencyValid(String currency) {

        try {
            Currency.valueOf(currency);
            return true;
        } catch (Exception ex) {

            return false;
        }
    }
}
