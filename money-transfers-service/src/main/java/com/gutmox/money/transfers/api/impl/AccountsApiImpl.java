package com.gutmox.money.transfers.api.impl;

import com.gutmox.money.transfers.api.AccountsApi;
import com.gutmox.money.transfers.api.domain.Account;
import com.gutmox.money.transfers.api.validations.AccountsValidations;
import com.gutmox.money.transfers.iban.IbanBuilder;
import com.gutmox.money.transfers.repositories.AccountsRepository;
import rx.Single;

import javax.inject.Inject;

public class AccountsApiImpl implements AccountsApi {

    private AccountsRepository accountRepository;

    private AccountsValidations accountsValidation;

    private IbanBuilder ibanBuilder;

    @Inject
    public AccountsApiImpl(AccountsRepository accountRepository,
                           AccountsValidations accountsValidation,
                           IbanBuilder ibanBuilder) {

        this.accountRepository = accountRepository;

        this.accountsValidation = accountsValidation;

        this.ibanBuilder = ibanBuilder;
    }

    @Override
    public Single<String> createAccount(Account account) {

        return accountsValidation.isValid(account).flatMap(isValid -> {

            String accountNumber = ibanBuilder.getNextIbanAccount();

            return accountRepository.save(accountNumber, account.getAccountAsJson());

        });
    }

    @Override
    public Single<Account> getAccount(String accountIdentifier) {

        return accountRepository.get(accountIdentifier).map(json -> Account.builder().withAccountAsJson(json).build());
    }

    @Override
    public void withDrawBalance(String sourceAccountId, Double amount) {

        getAccount(sourceAccountId).subscribe(account -> {

            account.setBalance(account.getBalance() - amount);

            accountRepository.save(sourceAccountId, account.getAccountAsJson());
        });

    }

    @Override
    public void addToBalance(String targetAccountId, Double amount) {

        getAccount(targetAccountId).subscribe(account -> {

            account.setBalance(account.getBalance() + amount);

            accountRepository.save(targetAccountId, account.getAccountAsJson());
        });

    }
}
