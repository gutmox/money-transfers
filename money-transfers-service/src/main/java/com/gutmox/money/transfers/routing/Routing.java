package com.gutmox.money.transfers.routing;

import com.gutmox.money.transfers.handlers.AccountsCreationHandler;
import com.gutmox.money.transfers.handlers.AccountsGetsHandler;
import com.gutmox.money.transfers.handlers.StatusHandler;
import com.gutmox.money.transfers.handlers.TransfersHandler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.ext.web.Router;
import io.vertx.rxjava.ext.web.handler.BodyHandler;
import io.vertx.rxjava.ext.web.handler.ResponseTimeHandler;
import io.vertx.rxjava.ext.web.handler.StaticHandler;

public class Routing {

    private static final Logger LOGGER = LoggerFactory.getLogger(Routing.class);

    private StatusHandler statusHandler;

    private AccountsCreationHandler accountsCreationHandler;

    private AccountsGetsHandler accountsGetsHandler;

    private TransfersHandler transfersHandler;


    private Router router;

    public Routing(StatusHandler statusHandler,
                   AccountsCreationHandler accountsCreationHandler,
                   AccountsGetsHandler accountsGetsHandler,
                   TransfersHandler transfersHandler) {

        this.statusHandler = statusHandler;
        this.accountsCreationHandler = accountsCreationHandler;
        this.accountsGetsHandler = accountsGetsHandler;
        this.transfersHandler = transfersHandler;
    }

    public Router get() {
        long bodyLimit = 1024;

        router.route().handler(ResponseTimeHandler.create());

        router.get("/status").handler(statusHandler);

        router.post("/accounts").handler(BodyHandler.create().setBodyLimit(bodyLimit * bodyLimit));
        router.post("/accounts").handler(accountsCreationHandler);
        router.get("/accounts/:accountIdentifier").handler(accountsGetsHandler);

        router.post("/transfer").handler(BodyHandler.create().setBodyLimit(bodyLimit * bodyLimit));
        router.post("/transfer").handler(transfersHandler);

        router.route().handler(BodyHandler.create().setBodyLimit(bodyLimit * bodyLimit));
        // Enable the web console to access swagger ui
        router.route().handler(StaticHandler.create());

        LOGGER.debug("Routing get");
        return router;
    }

    public void setRouter(Router router) {
        this.router = router;
    }
}
