package com.gutmox.money.transfers.handlers;

import com.gutmox.money.transfers.api.AccountsApi;
import com.gutmox.money.transfers.api.impl.AccountsApiImpl;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.ext.web.RoutingContext;

import javax.inject.Inject;

public class AccountsGetsHandler implements Handler<RoutingContext> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountsGetsHandler.class);

    private AccountsApi accountsApi;

    @Inject
    public AccountsGetsHandler(AccountsApiImpl accountsApi) {
        this.accountsApi = accountsApi;
    }

    @Override
    public void handle(RoutingContext context) {

        String accountIdentifier = context.request().getParam("accountIdentifier");
        LOGGER.info("accountIdentifier: " + accountIdentifier);

        accountsApi.getAccount(accountIdentifier).subscribe(account -> {

            LOGGER.info("Result : " + account);

            context.response().putHeader("content-type", "application/json")
                    .end(account.getAccountAsJson().encode());
        });
    }
}
