package com.gutmox.money.transfers.api.impl;

import com.gutmox.money.transfers.api.AccountsApi;
import com.gutmox.money.transfers.api.TransfersApi;
import com.gutmox.money.transfers.api.domain.Transaction;
import com.gutmox.money.transfers.api.validations.TransactionValidations;
import com.gutmox.money.transfers.repositories.TransactionsRepository;
import rx.Single;

import javax.inject.Inject;
import java.util.UUID;

public class TransfersApiImpl implements TransfersApi {

    private TransactionsRepository transactionsRepository;

    private TransactionValidations transactionValidations;

    private AccountsApi accountsApi;

    @Inject
    public TransfersApiImpl(TransactionsRepository transactionsRepository,
                            TransactionValidations transactionValidations,
                            AccountsApiImpl accountsApi) {

        this.transactionsRepository = transactionsRepository;

        this.transactionValidations = transactionValidations;

        this.accountsApi = accountsApi;
    }

    @Override
    public Single<String> doTransfer(Transaction transaction) {

        String transactionIdentifier = UUID.randomUUID().toString();

        transactionValidations.isValid(transaction).subscribe(isValid -> {

            transactionsRepository.save(transactionIdentifier, transaction.getTransactionAsJson());

            if (isValid){

                accountsApi.withDrawBalance(transaction.getSourceAccountId(),
                        transaction.getAmount());

                accountsApi.addToBalance(transaction.getTargetAccountId(),
                        transaction.getAmount());
            }
        });

        return Single.just(transactionIdentifier);
    }

}