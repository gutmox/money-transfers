package com.gutmox.money.transfers.api;

import com.gutmox.money.transfers.api.domain.Account;
import rx.Single;

public interface AccountsApi {

    Single<String> createAccount(Account account);

    Single<Account> getAccount(String accountIdentifier);

    void withDrawBalance(String sourceAccountId, Double amount);

    void addToBalance(String targetAccountId, Double amount);
}
