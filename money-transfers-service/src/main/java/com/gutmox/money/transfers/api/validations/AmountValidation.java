package com.gutmox.money.transfers.api.validations;

class AmountValidation {

    private AmountValidation() {
    }

    static boolean isAmountValid(Double amount) {

        return (amount > 0);
    }
}
