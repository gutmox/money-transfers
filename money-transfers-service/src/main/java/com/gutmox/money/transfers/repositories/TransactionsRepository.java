package com.gutmox.money.transfers.repositories;

import com.gutmox.money.transfers.repositories.mongo.config.MongoConfiguration;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.ext.mongo.MongoClient;

import javax.inject.Inject;

public class TransactionsRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionsRepository.class);

    private MongoClient mongoClient;

    private String collection = "transactions";

    @Inject
    public TransactionsRepository() {
        this(MongoConfiguration.init());
    }

    public TransactionsRepository(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    public void save(String transactionIdentifier, JsonObject transaction) {

        LOGGER.info("Tx id:" + transactionIdentifier);

        transaction.put("_id", transactionIdentifier);

        mongoClient.rxSave(collection, transaction).subscribe();
    }
}
