package com.gutmox.money.transfers.api.domain;

import io.vertx.core.json.JsonObject;

import java.util.Objects;

public class Transaction {

    private JsonObject transactionAsJson;

    private Transaction(JsonObject accountAsJson) {
        this.transactionAsJson = accountAsJson;
    }

    public String getStatus() {
        return transactionAsJson.getString("status");
    }

    public void setStatus(String status) {

        transactionAsJson.put("status", status);
    }

    public String getSourceAccountId() {
        return transactionAsJson.getString("source_account_id");
    }

    public String getTargetAccountId() {
        return transactionAsJson.getString("target_account_id");
    }

    public Double getAmount() {
        return transactionAsJson.getDouble("amount");
    }

    public void setAmount(Double amount) {
        transactionAsJson.put("amount", amount);
    }

    public String getCurrency() {
        return transactionAsJson.getString("currency");
    }

    public String getTransactionId() {
        return transactionAsJson.getString("transaction_id");
    }

    public void setTransactionId(String transactionId) {
        this.transactionAsJson.put("transaction_id", transactionId);
    }

    public JsonObject getTransactionAsJson() {
        return transactionAsJson;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction transfer = (Transaction) o;
        return Objects.equals(transactionAsJson, transfer.transactionAsJson);
    }

    @Override
    public int hashCode() {

        return Objects.hash(transactionAsJson);
    }


    public static TransactionBuilder builder() {
        return new TransactionBuilder();
    }

    public static final class TransactionBuilder {
        private JsonObject transactionAsJson;

        public TransactionBuilder withTransactionAsJson(JsonObject transactionAsJson) {
            this.transactionAsJson = transactionAsJson;
            return this;
        }

        public Transaction build() {
            Transaction transaction = new Transaction(null);
            transaction.transactionAsJson = this.transactionAsJson;
            return transaction;
        }
    }
}
