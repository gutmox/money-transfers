package com.gutmox.money.transfers.api.validations;

import com.google.inject.Inject;
import com.gutmox.money.transfers.api.AccountsApi;
import com.gutmox.money.transfers.api.domain.Account;
import com.gutmox.money.transfers.api.domain.Transaction;
import com.gutmox.money.transfers.api.impl.AccountsApiImpl;
import rx.Single;
import rx.functions.Func1;

import java.util.Objects;

public class TransactionValidations {

    private AccountsApi accountsApi;

    @Inject
    TransactionValidations(AccountsApiImpl accountsApi) {

        this.accountsApi = accountsApi;
    }

    public Single<Boolean> isValid(Transaction transaction) {

        Double amount = transaction.getAmount();

        if (! AmountValidation.isAmountValid(amount)){

            transaction.setStatus("Cancelled: Amount should be positive");

            return Single.just(false);
        }

        if (! CurrencyValidation.isCurrencyValid(transaction.getCurrency())){

            transaction.setStatus("Cancelled: Non Valid Currency");

            return Single.just(false);
        }

        return accountsApi.getAccount(transaction.getSourceAccountId()).map(account -> {

            if (isSourceAccountValid(transaction, account)) return false;

            Double balance = account.getBalance();

            return isSourceAccountBalanceValid(transaction, amount, balance);

        }).map(isTargetAccountValid(transaction));
    }



    private Func1<Boolean, Boolean> isTargetAccountValid(Transaction transaction) {
        return isSourceValid -> {

            if (isSourceValid) {

                accountsApi.getAccount(transaction.getTargetAccountId())
                        .map(Objects::isNull);
            }
            return true;
        };
    }

    private boolean isSourceAccountValid(Transaction transaction, Account account) {

        if (account == null) {

            transaction.setStatus("Cancelled: Non Existing Source Account");
            return true;
        }
        return false;
    }

    private Boolean isSourceAccountBalanceValid(Transaction transaction, Double amount, Double balance) {

        if (amount > balance) {

            transaction.setStatus("Cancelled: Out_of_balance");

            return false;
        } else {

            transaction.setStatus("Processed");
        }

        return true;
    }
}
