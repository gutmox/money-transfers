package com.gutmox.money.transfers.api.domain;

import io.vertx.core.json.JsonObject;

import java.util.Objects;

public class Account {

    private JsonObject accountAsJson;

    private Account(JsonObject accountAsJson) {
        this.accountAsJson = accountAsJson;
    }

    public String getName() {
        return accountAsJson.getString("name");
    }

    public String getCurrency() {
        return accountAsJson.getString("currency");
    }

    public Double getBalance() {
        return accountAsJson.getDouble("balance");
    }

    public void setBalance(Double balance) {
        accountAsJson.put("balance", balance);
    }

    public String getState() {
        return accountAsJson.getString("state");
    }

    public void setState(String state) {
        accountAsJson.put("state", state);
    }

    public String getAccountNumber() {
        return accountAsJson.getString("account_number");
    }

    public void setAccountNumber(String accountNumber) {
        accountAsJson.put("account_number", accountNumber);
    }

    public JsonObject getAccountAsJson() {
        return accountAsJson;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(accountAsJson, account.accountAsJson);
    }

    @Override
    public int hashCode() {

        return Objects.hash(accountAsJson);
    }

    public static AccountBuilder builder() {
        return new AccountBuilder();
    }

    public static final class AccountBuilder {
        private JsonObject accountAsJson;

        public AccountBuilder withAccountAsJson(JsonObject accountAsJson) {
            this.accountAsJson = accountAsJson;
            return this;
        }

        public Account build() {
            return new Account(accountAsJson);
        }
    }
}
