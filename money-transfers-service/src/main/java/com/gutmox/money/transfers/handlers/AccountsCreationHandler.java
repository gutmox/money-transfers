package com.gutmox.money.transfers.handlers;

import com.gutmox.money.transfers.api.AccountsApi;
import com.gutmox.money.transfers.api.domain.Account;
import com.gutmox.money.transfers.api.impl.AccountsApiImpl;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.ext.web.RoutingContext;

import javax.inject.Inject;

public class AccountsCreationHandler implements Handler<RoutingContext> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountsCreationHandler.class);

    private AccountsApi accountsApi;

    @Inject
    AccountsCreationHandler(AccountsApiImpl accountsApi) {
        this.accountsApi = accountsApi;
    }

    @Override
    public void handle(RoutingContext context) {

        LOGGER.info("Body : " + context.getBodyAsString());

        accountsApi.createAccount(Account.builder().withAccountAsJson(context.getBodyAsJson()).build()).subscribe(accountNumber -> {

            LOGGER.info("Result : " + accountNumber);

            context.response().putHeader("content-type", "application/json")
                    .end(context.getBodyAsJson()
                            .put("account_number", accountNumber)
                            .encode());
        });
    }
}
