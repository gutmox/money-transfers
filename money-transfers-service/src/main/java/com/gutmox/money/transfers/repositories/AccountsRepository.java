package com.gutmox.money.transfers.repositories;

import com.gutmox.money.transfers.repositories.mongo.config.MongoConfiguration;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.ext.mongo.MongoClient;
import rx.Single;

import javax.inject.Inject;

public class AccountsRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountsRepository.class);

    private MongoClient mongoClient;

    private String collection = "accounts";

    @Inject
    public AccountsRepository() {

        this(MongoConfiguration.init());
    }

    public AccountsRepository(MongoClient mongoClient) {

        this.mongoClient = mongoClient;
    }

    public Single<String> save(String accountNumber, JsonObject account) {

        LOGGER.info("accountNumber : " + accountNumber);

        account.put("_id", accountNumber);

        mongoClient.rxSave(collection, account).subscribe();

        return Single.just(accountNumber);
    }

    public Single<JsonObject> get(String accountNumber) {

        LOGGER.info("accountNumber : " + accountNumber);

        return mongoClient.rxFindOne(collection, new JsonObject().put("_id", accountNumber), null);
    }
}
