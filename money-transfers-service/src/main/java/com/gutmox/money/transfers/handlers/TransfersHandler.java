package com.gutmox.money.transfers.handlers;

import com.gutmox.money.transfers.api.TransfersApi;
import com.gutmox.money.transfers.api.domain.Transaction;
import com.gutmox.money.transfers.api.impl.TransfersApiImpl;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.ext.web.RoutingContext;

import javax.inject.Inject;

public class TransfersHandler implements Handler<RoutingContext> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransfersHandler.class);

    private TransfersApi transfersApi;

    @Inject
    public TransfersHandler(TransfersApiImpl transfersApi) {
        this.transfersApi = transfersApi;
    }

    @Override
    public void handle(RoutingContext context) {

        LOGGER.info("Body : " + context.getBodyAsString());

        JsonObject bodyAsJson = context.getBodyAsJson();

        transfersApi.doTransfer(Transaction.builder().withTransactionAsJson(bodyAsJson).build()).subscribe(transactionIdentifier -> {

            LOGGER.info("Result : " + transactionIdentifier);

            context.response().putHeader("content-type", "application/json")
                    .end(bodyAsJson
                            .put("transaction_id", transactionIdentifier)
                            .encode());
        });
    }
}
