package com.gutmox.money.transfers.verticles;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.gutmox.money.transfers.di.GuiceModule;
import com.gutmox.money.transfers.handlers.AccountsCreationHandler;
import com.gutmox.money.transfers.handlers.AccountsGetsHandler;
import com.gutmox.money.transfers.handlers.StatusHandler;
import com.gutmox.money.transfers.handlers.TransfersHandler;
import com.gutmox.money.transfers.repositories.mongo.config.MongoConfiguration;
import com.gutmox.money.transfers.routing.Routing;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.http.HttpServer;
import io.vertx.rxjava.ext.web.Router;
import rx.Observable;

public class MainVerticle extends io.vertx.rxjava.core.AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainVerticle.class);

    @Inject
    private StatusHandler statusHandler;

    @Inject
    private AccountsCreationHandler accountsCreationHandler;

    @Inject
    private AccountsGetsHandler accountsGetsHandler;

    @Inject
    private TransfersHandler transfersHandler;

    @Override
    public void start(final Future<Void> startedResult) {
        startServer().subscribe(
                t -> {
                },
                t -> LOGGER.error(t.getMessage()),
                () -> {
                    LOGGER.info(MainVerticle.class.getName() + " Running on " + getPort() + " !!!!!!! ");
                    startedResult.complete();
                }
        );
    }

    private Observable<HttpServer> startServer() {
        MongoConfiguration.setVertx(vertx);
        Guice.createInjector(new GuiceModule(vertx)).injectMembers(this);
        HttpServerOptions options = new HttpServerOptions().setCompressionSupported(true);
        HttpServer httpServer = vertx.createHttpServer(options);
        Integer configuredPort = getPort();
        return httpServer.requestHandler(getRouting()::accept).rxListen(configuredPort).toObservable();
    }

    private Integer getPort() {
        return this.getVertx().getOrCreateContext().config().getInteger("http.port");
    }

    private Router getRouting(){
        Routing routing = new Routing(statusHandler, accountsCreationHandler, accountsGetsHandler, transfersHandler);
        routing.setRouter(Router.router(vertx));
        return routing.get();
    }
}
