package com.gutmox.money.transfers.api.validations;

import com.gutmox.money.transfers.api.domain.Account;
import rx.Single;

public class AccountsValidations {

    public Single<Boolean> isValid(Account account) {

        if (! CurrencyValidation.isCurrencyValid(account.getCurrency())){

            account.setState("Invalid: Currency not supported");

            return Single.just(false);
        }

        if (! AmountValidation.isAmountValid(account.getBalance())){

            account.setState("Invalid: Balance should be positive");

            return Single.just(false);
        }

        return Single.just(true);
    }
}
