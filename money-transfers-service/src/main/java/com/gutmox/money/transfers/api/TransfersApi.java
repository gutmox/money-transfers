package com.gutmox.money.transfers.api;

import com.gutmox.money.transfers.api.domain.Transaction;
import rx.Single;

public interface TransfersApi {

    Single<String> doTransfer(Transaction transaction);

}
