package com.gutmox.money.transfers.api.validations;

import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class AmountValidationTest {

    @Test
    public void should_validate() {

        boolean amountValid = AmountValidation.isAmountValid(15.0);

        assertThat(amountValid).isEqualTo(true);
    }

    @Test
    public void should_be_invalid() {

        boolean amountValid = AmountValidation.isAmountValid(-15.0);

        assertThat(amountValid).isEqualTo(false);
    }

}