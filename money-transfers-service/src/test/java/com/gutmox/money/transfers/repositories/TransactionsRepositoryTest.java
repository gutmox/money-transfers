package com.gutmox.money.transfers.repositories;

import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.mongo.MongoClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import rx.Single;

import java.util.UUID;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TransactionsRepositoryTest {

    private TransactionsRepository transactionsRepository;

    @Mock
    private MongoClient mongoClient;


    private String transactionIdentifier = UUID.randomUUID().toString();

    private JsonObject transaction = new JsonObject()
            .put("request_identifier", 1)
            .put("source_account_id", "AT381904531941388853")
            .put("target_account_id", "AT381904531941388854")
            .put("currency", "EUR")
            .put("reference", "Transaction")
            .put("amount", 5)
            .put("transaction_id", transactionIdentifier);

    @Before
    public void setUp() {

        transactionsRepository = new TransactionsRepository(mongoClient);
    }


    @Test
    public void should_save_account() {

        doReturn(Single.just("id")).when(mongoClient).rxSave("transactions", transaction);

        transactionsRepository.save(transactionIdentifier, transaction);

        verify(mongoClient).rxSave("transactions", transaction);
    }
}