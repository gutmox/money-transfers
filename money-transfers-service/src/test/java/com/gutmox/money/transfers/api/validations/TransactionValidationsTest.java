package com.gutmox.money.transfers.api.validations;

import com.gutmox.money.transfers.api.domain.Account;
import com.gutmox.money.transfers.api.domain.Transaction;
import com.gutmox.money.transfers.api.impl.AccountsApiImpl;
import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import rx.Single;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class TransactionValidationsTest {

    @Mock
    private AccountsApiImpl accountsApi;

    private TransactionValidations transactionValidations;

    private Transaction transaction = Transaction.builder().withTransactionAsJson(new JsonObject()
            .put("request_identifier", 1)
            .put("source_account_id", "AT381904531941388853")
            .put("target_account_id", "AT381904531941388854")
            .put("currency", "EUR")
            .put("reference", "Transaction")
            .put("amount", 5)).build();

    private Account source_account = Account.builder().withAccountAsJson(new JsonObject()
                .put("_id", "AT381904531941388853")
                .put("name", "MyAccount")
                .put("currency", "EUR")
                .put("state", "active")
                .put("balance", 15.0)).build();

    private Account target_account = Account.builder().withAccountAsJson(new JsonObject()
            .put("_id", "AT381904531941388854")
            .put("name", "MyAccount")
            .put("currency", "EUR")
            .put("state", "active")
            .put("balance", 15.0)).build();

    private Transaction transactionInvalidCurrency = Transaction.builder().withTransactionAsJson(new JsonObject()
            .put("request_identifier", 1)
            .put("source_account_id", "AT381904531941388853")
            .put("target_account_id", "AT381904531941388854")
            .put("currency", "Gibberish")
            .put("reference", "Transaction")
            .put("amount", 5)).build();

    @Before
    public void setUp(){
        transactionValidations = new TransactionValidations(accountsApi);
    }

    @Test
    public void transaction_should_be_valid(){

        doReturn(Single.just(source_account)).when(accountsApi).getAccount("AT381904531941388853");

        doReturn(Single.just(target_account)).when(accountsApi).getAccount("AT381904531941388854");

        transactionValidations.isValid(transaction).subscribe(isValid ->{

            assertThat(isValid).isEqualTo(true);
        });
    }

    @Test
    public void transaction_should_be_invalid_when_not_finding_source(){

        doReturn(Single.just(null)).when(accountsApi).getAccount("AT381904531941388853");

        transactionValidations.isValid(transaction).subscribe(isValid ->{

            assertThat(isValid).isEqualTo(true);
        });
    }

    @Test
    public void transaction_should_be_invalid_when_not_finding_target(){

        doReturn(Single.just(source_account)).when(accountsApi).getAccount("AT381904531941388853");

        doReturn(Single.just(null)).when(accountsApi).getAccount("AT381904531941388854");

        transactionValidations.isValid(transaction).subscribe(isValid ->{

            assertThat(isValid).isEqualTo(true);
        });
    }


    @Test
    public void should_not_validate_currency() {

        Single<Boolean> valid = transactionValidations.isValid(transactionInvalidCurrency);

        valid.subscribe(isValid -> {

            assertThat(isValid).isEqualTo(false);
        });
    }
}