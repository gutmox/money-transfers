package com.gutmox.money.transfers.iban;

import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class IbanBuilderTest {

    private IbanBuilder ibanBuilder = new IbanBuilder();


    @Test
    public void should_return_valid_account_number(){


        String nextIbanAccount = ibanBuilder.getNextIbanAccount();

        assertThat(nextIbanAccount).isNotBlank();
        assertThat(nextIbanAccount.length()).isBetween(15, 20);
    }
}