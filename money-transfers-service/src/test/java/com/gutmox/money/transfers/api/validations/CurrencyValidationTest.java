package com.gutmox.money.transfers.api.validations;

import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class CurrencyValidationTest {
    
    @Test
    public void should_be_valid() {

        boolean currencyValid = CurrencyValidation.isCurrencyValid("EUR");

        assertThat(currencyValid).isEqualTo(true);
    }

    @Test
    public void should_be_invalid() {

        boolean currencyValid = CurrencyValidation.isCurrencyValid("Gibberish");

        assertThat(currencyValid).isEqualTo(false);
    }
}