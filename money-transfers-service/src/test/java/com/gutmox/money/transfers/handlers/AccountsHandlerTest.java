package com.gutmox.money.transfers.handlers;

import com.gutmox.money.transfers.api.domain.Account;
import com.gutmox.money.transfers.api.impl.AccountsApiImpl;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.core.http.HttpServerResponse;
import io.vertx.rxjava.ext.web.RoutingContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import rx.Single;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AccountsHandlerTest {

    private AccountsCreationHandler accountsHandler;

    @Mock
    private RoutingContext context;

    @Mock
    private HttpServerResponse response;

    @Mock
    private AccountsApiImpl accountsApi;

    private JsonObject jsonObject;

    private Account account;

    private String accountNumber = "AT661904649466803107";

    @Before
    public void setUp(){

        accountsHandler = new AccountsCreationHandler(accountsApi);

        jsonObject = new JsonObject()
                .put("name", "MyAccount")
                .put("currency", "EUR")
                .put("state", "active")
                .put("balance", 5.0);

        account = Account.builder().withAccountAsJson(jsonObject).build();
    }

    @Test
    public void should_return_account_created() {

        given();

        accountsHandler.handle(context);

        verify(response).end(new JsonObject()
                .put("name", "MyAccount")
                .put("currency", "EUR")
                .put("state", "active")
                .put("balance", 5.0)
                .put("account_number", accountNumber)
                .encode());
    }

    @Test
    public void should_pass_params_to_api() {

        given();

        accountsHandler.handle(context);

        verify(accountsApi).createAccount(account);
    }

    private void given() {

        doReturn(response).when(context).response();

        doReturn(response).when(response).putHeader("content-type", "application/json");

        doReturn(jsonObject).when(context).getBodyAsJson();

        doReturn(Single.just(accountNumber)).when(accountsApi).createAccount(account);
    }
}