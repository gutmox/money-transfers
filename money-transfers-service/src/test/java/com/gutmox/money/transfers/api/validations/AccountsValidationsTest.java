package com.gutmox.money.transfers.api.validations;

import com.gutmox.money.transfers.api.domain.Account;
import io.vertx.core.json.JsonObject;
import org.junit.Test;
import rx.Single;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class AccountsValidationsTest {

    private AccountsValidations accountsValidations = new AccountsValidations();

    private JsonObject validAccount = new JsonObject()
            .put("name", "MyAccount")
            .put("currency", "EUR")
            .put("state", "active")
            .put("balance", 15.0);

    private JsonObject inValidAccount = new JsonObject()
            .put("name", "MyAccount")
            .put("currency", "Gibberish")
            .put("state", "active")
            .put("balance", 15.0);

    private JsonObject inValidBalanceAccount = new JsonObject()
            .put("name", "MyAccount")
            .put("currency", "EUR")
            .put("state", "active")
            .put("balance", -4);

    @Test
    public void should_be_valid() {

        Single<Boolean> valid = accountsValidations.isValid(Account.builder().withAccountAsJson(validAccount).build());

        valid.subscribe(isValid -> {

            assertThat(isValid).isEqualTo(true);
        });
    }

    @Test
    public void should_not_validate_currency() {

        Single<Boolean> valid = accountsValidations.isValid(Account.builder().withAccountAsJson(inValidAccount).build());

        valid.subscribe(isValid -> {

            assertThat(isValid).isEqualTo(false);
        });
    }

    @Test
    public void should_not_validate_balance() {

        Single<Boolean> valid = accountsValidations.isValid(Account.builder().withAccountAsJson(inValidBalanceAccount).build());

        valid.subscribe(isValid -> {

            assertThat(isValid).isEqualTo(false);
        });
    }

}