package com.gutmox.money.transfers.handlers;

import com.gutmox.money.transfers.api.domain.Transaction;
import com.gutmox.money.transfers.api.impl.TransfersApiImpl;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.core.http.HttpServerResponse;
import io.vertx.rxjava.ext.web.RoutingContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import rx.Single;

import java.util.UUID;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TransfersHandlerTest {

    private TransfersHandler transfersHandler;

    @Mock
    private RoutingContext context;

    @Mock
    private HttpServerResponse response;

    @Mock
    private TransfersApiImpl transfersApi;

    private String transactionIdentifier = UUID.randomUUID().toString();

    JsonObject  jsonObject = new JsonObject()
            .put("request_identifier", 1)
            .put("source_account_id", "AT381904531941388853")
            .put("target_account_id", "AT381904531941388854")
            .put("currency", "EUR")
            .put("reference", "Transaction")
            .put("amount", 5);

    private Transaction transaction = Transaction.builder().withTransactionAsJson(jsonObject).build();

    @Before
    public void setUp(){
        transfersHandler = new TransfersHandler(transfersApi);
    }

    @Test
    public void should_pass_params_to_api() {

        given();

        transfersHandler.handle(context);

        verify(transfersApi).doTransfer(transaction);
    }

    @Test
    public void should_return_account_created() {

        given();

        transfersHandler.handle(context);

        verify(response).end(new JsonObject()
                .put("request_identifier", 1)
                .put("source_account_id", "AT381904531941388853")
                .put("target_account_id", "AT381904531941388854")
                .put("currency", "EUR")
                .put("reference", "Transaction")
                .put("amount", 5)
                .put("transaction_id", transactionIdentifier)
                .encode());
    }


    private void given() {

        doReturn(response).when(context).response();

        doReturn(response).when(response).putHeader("content-type", "application/json");

        doReturn(jsonObject.toString()).when(context).getBodyAsString();

        doReturn(jsonObject).when(context).getBodyAsJson();

        doReturn(Single.just(transactionIdentifier)).when(transfersApi).doTransfer(transaction);
    }

}