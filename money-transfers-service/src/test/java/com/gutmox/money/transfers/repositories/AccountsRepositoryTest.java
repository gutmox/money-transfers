package com.gutmox.money.transfers.repositories;

import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.mongo.MongoClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import rx.Single;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class AccountsRepositoryTest {

    private AccountsRepository accountsRepository;

    @Mock
    private MongoClient mongoClient;

    private String accountNumber = "AT661904649466803107";

    private JsonObject account = new JsonObject()
            .put("_id", "accountNumber")
            .put("name", "MyAccount")
            .put("currency", "EUR")
            .put("state", "active")
            .put("balance", 5.0);


    @Before
    public void setUp() {

        accountsRepository = new AccountsRepository(mongoClient);
    }

    @Test
    public void should_save_account() {

        doReturn(Single.just("id")).when(mongoClient).rxSave("accounts", account);

        accountsRepository.save(accountNumber, account);

        verify(mongoClient).rxSave("accounts", account);
    }

    @Test
    public void should_return_account_number() {

        doReturn(Single.just("id")).when(mongoClient).rxSave("accounts", account);

        Single<String> savedAccountNumber = accountsRepository.save(accountNumber, account);

        savedAccountNumber.subscribe(res -> {

            assertThat(res).isEqualTo(accountNumber);
        });
    }
}