package com.gutmox.money.transfers.api;

import com.gutmox.money.transfers.api.domain.Account;
import com.gutmox.money.transfers.api.impl.AccountsApiImpl;
import com.gutmox.money.transfers.api.validations.AccountsValidations;
import com.gutmox.money.transfers.iban.IbanBuilder;
import com.gutmox.money.transfers.repositories.AccountsRepository;
import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import rx.Single;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class AccountsApiTest {

    private AccountsApi accountsApi;

    @Mock
    private AccountsRepository accountRepository;

    @Mock
    private AccountsValidations accountsValidation;

    @Mock
    private IbanBuilder ibanBuilder;

    private JsonObject jsonObject;

    private Account account;

    private String accountNumber = "AT021904535649333520";

    @Before
    public void setUp() {


        accountsApi = new AccountsApiImpl(accountRepository, accountsValidation, ibanBuilder);

        jsonObject = new JsonObject()
                .put("name", "MyAccount")
                .put("currency", "EUR")
                .put("state", "active")
                .put("balance", 15.0);

        account = Account.builder().withAccountAsJson(jsonObject).build();
    }

    @Test
    public void create_account_should_invoke_repository() {

        doReturn(Single.just(true)).when(accountsValidation).isValid(account);

        doReturn(accountNumber).when(ibanBuilder).getNextIbanAccount();

        try {
            accountsApi.createAccount(account).doOnError(error -> {}).subscribe(accountNumber -> {});
        } catch (Exception ex){}

        verify(accountRepository).save(accountNumber, account.getAccountAsJson());
    }

    @Test
    public void get_account_should_invoke_repository() {

        doReturn(Single.just(jsonObject)).when(accountRepository).get(accountNumber);

        accountsApi.getAccount(accountNumber);

        verify(accountRepository).get(accountNumber);
    }

    @Test
    public void withdraw_should_invoke_repository() {

        doReturn(Single.just(jsonObject)).when(accountRepository).get(accountNumber);

        accountsApi.withDrawBalance(accountNumber, 10.0);

        verify(accountRepository).save(eq(accountNumber), any());
    }

    @Test
    public void withdraw_should_withdraw_from_balance() {

        doReturn(Single.just(jsonObject)).when(accountRepository).get(accountNumber);

        accountsApi.withDrawBalance(accountNumber, 10.0);

        JsonObject put = new JsonObject()
                .put("name", "MyAccount")
                .put("currency", "EUR")
                .put("state", "active")
                .put("balance", 5.0);

        verify(accountRepository).save(accountNumber, put);
    }

    @Test
    public void add_balance_should_invoke_repository() {

        doReturn(Single.just(jsonObject)).when(accountRepository).get(accountNumber);

        accountsApi.addToBalance(accountNumber, 10.0);

        verify(accountRepository).save(eq(accountNumber), any());
    }

    @Test
    public void add_balance_should_su_money() {

        doReturn(Single.just(jsonObject)).when(accountRepository).get(accountNumber);

        accountsApi.addToBalance(accountNumber, 10.0);

        JsonObject put = new JsonObject()
                .put("name", "MyAccount")
                .put("currency", "EUR")
                .put("state", "active")
                .put("balance", 25.0);

        verify(accountRepository).save(accountNumber, put);
    }
}