package com.gutmox.money.transfers.routing;

import com.gutmox.money.transfers.handlers.AccountsCreationHandler;
import com.gutmox.money.transfers.handlers.AccountsGetsHandler;
import com.gutmox.money.transfers.handlers.StatusHandler;
import com.gutmox.money.transfers.handlers.TransfersHandler;
import io.vertx.rxjava.ext.web.Route;
import io.vertx.rxjava.ext.web.Router;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RoutingTest {

    private Routing routing;

    @Mock
    private StatusHandler statusHandler;

    @Mock
    private AccountsCreationHandler accountsCreationHandler;

    @Mock
    private AccountsGetsHandler accountsGetsHandler;

    @Mock
    private TransfersHandler transfersHandler;

    @Mock
    private Router router;

    @Mock
    private Route route;

    @Before
    public void setUp() {
        routing = new Routing(statusHandler, accountsCreationHandler, accountsGetsHandler, transfersHandler);

        routing.setRouter(router);

        doReturn(route).when(router).route();

        doReturn(route).when(router).get("/status");

        doReturn(route).when(router).post("/accounts");

        doReturn(route).when(router).get("/accounts/:accountIdentifier");

        doReturn(route).when(router).post("/transfer");
    }

    @Test
    public void router_should_route_status_handler() {

        Router router = routing.get();

        List<Route> routes = router.getRoutes();

        assertThat(routes.stream()
                .anyMatch(route -> route.getPath() != null
                        && route.getPath().equals("/status")));

        verify(route).handler(statusHandler);
    }

    @Test
    public void router_should_route_accounts_creation_handler() {

        Router router = routing.get();

        List<Route> routes = router.getRoutes();

        assertThat(routes.stream()
                .anyMatch(route -> route.getPath() != null
                        && route.getPath().equals("/accounts")));

        verify(route).handler(accountsCreationHandler);
    }

    @Test
    public void router_should_route_accounts_get_handler() {

        Router router = routing.get();

        List<Route> routes = router.getRoutes();

        assertThat(routes.stream()
                .anyMatch(route -> route.getPath() != null
                        && route.getPath().equals("/accounts/:accountIdentifier")));

        verify(route).handler(accountsGetsHandler);
    }


    @Test
    public void router_should_route_transfers_handler() {

        Router router = routing.get();

        List<Route> routes = router.getRoutes();

        assertThat(routes.stream()
                .anyMatch(route -> route.getPath() != null
                        && route.getPath().equals("/transfer")));

        verify(route).handler(accountsCreationHandler);
    }
}