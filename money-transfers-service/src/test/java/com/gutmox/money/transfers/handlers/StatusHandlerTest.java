package com.gutmox.money.transfers.handlers;

import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.core.http.HttpServerResponse;
import io.vertx.rxjava.ext.web.RoutingContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class StatusHandlerTest {

    private StatusHandler statusHandler;

    @Mock
    private RoutingContext context;

    @Mock
    private HttpServerResponse response;

    @Before
    public void setUp(){
        statusHandler = new StatusHandler();
    }

    @Test
    public void status_should_return_OK() {

        doReturn(response).when(context).response();

        doReturn(response).when(response).putHeader("content-type", "application/json");

        statusHandler.handle(context);

        verify(response).end(new JsonObject().put("status", "Show must go on").encode());
    }
}